//
// Created by yuanYue on 2021/8/10.
//

#ifndef CMDKEY_TOOL_MYWINDOW_H
#define CMDKEY_TOOL_MYWINDOW_H

#include <QMainWindow>


QT_BEGIN_NAMESPACE
namespace Ui { class MyWindow; }
QT_END_NAMESPACE

class MyWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MyWindow(QWidget *parent = nullptr);

    ~MyWindow() override;
public slots:
    void slotCommit();
private:
    Ui::MyWindow *ui;
};


#endif //CMDKEY_TOOL_MYWINDOW_H
